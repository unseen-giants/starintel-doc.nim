# Package

version     = "0.6.0"
author      = "nsaspy"
description = "Parse and handle starintel docs"
license     = "MIT"
srcDir       = "src"
# Deps

requires "nim >= 1.6.0"
requires "ulid"
